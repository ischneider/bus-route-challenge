package com.ischneider.busroute.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@EqualsAndHashCode
public class RouteResponse {

    @JsonProperty("dep_sid")
    private Integer departure;

    @JsonProperty("arr_sid")
    private Integer arrival;

    @JsonProperty("direct_bus_route")
    private Boolean directBusRoute;
}
