package com.ischneider.busroute.service;

import com.ischneider.busroute.model.RouteResponse;
import com.ischneider.busroute.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RouteService {

    @Autowired
    private RouteRepository repository;

    public RouteResponse getDirectRoute(Integer departure, Integer arrival) {
        return new RouteResponse(
                departure,
                arrival,
                repository.hasRoute( departure, arrival )
        );
    }


}
