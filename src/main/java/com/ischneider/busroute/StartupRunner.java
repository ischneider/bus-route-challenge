package com.ischneider.busroute;

import com.ischneider.busroute.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class StartupRunner implements CommandLineRunner {

    @Autowired
    private RouteRepository repository;

    @Override
    public void run(String... args) {
        repository.postRoutes( args[0] );
    }

}
