package com.ischneider.busroute.repository;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RouteRepository {

    private List<List<Integer>> routes;

    public void postRoutes(String filename) {
        try {
            routes = Files.lines( Paths.get( filename ))
                    .skip( 1 )
                    .map( this::getStations )
                    .collect( Collectors.toList() );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean hasRoute(Integer departure, Integer arrival) {
        return routes.stream()
                .anyMatch( route -> route.contains( departure ) && route.contains( arrival ) );
    }

    private List<Integer> getStations(String route) {
        return Arrays.stream( route.split(" ") )
                .skip( 1 )
                .map( Integer::parseInt )
                .collect( Collectors.toList() );
    }

}
