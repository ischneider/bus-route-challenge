package com.ischneider.busroute.controller;

import com.ischneider.busroute.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("api/direct")
public class RouteController {

    @Autowired
    private RouteService service;

    @GetMapping
    public ResponseEntity<?> getDirectRoute(@RequestParam("dep_sid") Integer departure, @RequestParam("arr_sid") Integer arrival) {
        return ResponseEntity
                .ok()
                .body( service.getDirectRoute( departure, arrival ) );
    }

}
