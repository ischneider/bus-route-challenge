package com.ischneider.busroute;

import com.ischneider.busroute.repository.RouteRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StartupRunnerTest {

    @Mock
    private RouteRepository repository;
    @InjectMocks
    private StartupRunner runner;

    @Test
    public void run() {
        String filename = "example";

        runner.run( filename );

        Mockito.verify( repository ).postRoutes( filename );
    }
}