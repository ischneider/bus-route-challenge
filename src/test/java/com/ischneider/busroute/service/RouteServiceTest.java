package com.ischneider.busroute.service;

import com.ischneider.busroute.model.RouteResponse;
import com.ischneider.busroute.repository.RouteRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class RouteServiceTest {

    @Mock
    private RouteRepository repository;
    @InjectMocks
    private RouteService service;

    @Test
    public void getDirectRouteWhenTrue() {
        Integer departure = 148;
        Integer arrival  = 17;
        Mockito.when( repository.hasRoute( departure, arrival ) )
                .thenReturn( true );
        RouteResponse expected = new RouteResponse( departure, arrival, true );

        RouteResponse response = service.getDirectRoute( departure, arrival );

        assertEquals( response, expected );
    }

    @Test
    public void getDirectRouteWhenFalse() {
        Integer departure = 148;
        Integer arrival  = 17;
        Mockito.when( repository.hasRoute( departure, arrival ) )
                .thenReturn( false );
        RouteResponse expected = new RouteResponse( departure, arrival, false );

        RouteResponse response = service.getDirectRoute( departure, arrival );

        assertEquals( response, expected );
    }
}